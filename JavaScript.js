let firstNum;
let secondNum;
let mathOp;

firstNum = numCheckCorrectness();
secondNum = numCheckCorrectness();

mathOp = mathOpCheckCorrectness();

function mathOpCheckCorrectness() {
    let op;
    do {
        op = prompt(`Виберіть математичну дію яку потрібно виконати: "+", "-" , "*" , "/"`);
    } while (!(op === "+" || op === "-" || op === "*" || op === "/"));
    return op;
}

function numCheckCorrectness() {
    let num;
    do {
        num = prompt(`Введіть цифру`);
    } while (!num || isNaN(num));

    return +num;
}

function mathFunction(num1, num2, op) {
    if (op === "+") {
        return +num1 + +num2;
    }
    if (op === "-") {
        return num1 - num2;
    }
    if (op === "/") {
        return num1 / num2;
    }
    if (op === "*") {
        return num1 * num2;
    }
}

console.log(mathFunction(firstNum, secondNum, mathOp));
